import React, { useState } from "react";

export default function App() {
  const [name, setName] = useState("");
  const [todos, setTodos] = useState([]);
  const [editIndex, setEditIndex] = useState(-1);

  const handle = (event) => {
    setName(event.target.value);
  };

  const deleteTodo = (index) => {
    const filteredTodos = todos.filter((_, todoIndex) => todoIndex !== index);
    setTodos(filteredTodos);
    setEditIndex(-1);
  };

  const addTodo = () => {
    if (editIndex === -1) {
      setTodos([...todos, name]);
    } else {
      const updatedTodos = [...todos];
      updatedTodos[editIndex] = name;
      setTodos(updatedTodos);
      setEditIndex(-1);
    }
    setName("");
  };

  const editTodo = (index) => {
    setName(todos[index]);
    setEditIndex(index);
  };

  return (
    <div className="py-5 App">
      <div className="container">
        <div className="row ">
          <div className="col-10">
            <input
              type="text"
              placeholder="enter a todo ..."
              onChange={handle}
              className="form-control"
              value={name}
            />
          </div>
          <div className="col-2">
            <button className="btn btn-info d-block w-100" onClick={addTodo}>
              {editIndex === -1 ? "Add" : "Update"}
            </button>
          </div>
        </div>
        <div className="row mt-3">
          {todos.map((item, index) => {
            return (
              <div
                key={index}
                className="col-10 offset-1 d-flex justify-content-between border my-2 p-1"
              >
                <h2>{item}</h2>
                <div className="">
                  <button
                    onClick={() => deleteTodo(index)}
                    className="btn btn-danger"
                  >
                    Delete
                  </button>
                  <button
                    onClick={() => editTodo(index)}
                    className="btn btn-secondary ml-2"
                  >
                    Edit
                  </button>
                </div>
                <h2 className="mark">{index}</h2>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
